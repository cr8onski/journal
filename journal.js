const entries = [
    {
        time: '1:00AM',
        action: 'cath',
        amount: '6 oz',
        details: 'why'
    },
    {
        time: '2:00AM',
        action: 'cath',
        amount: '6 oz',
        details: 'why'
    },
    {
        time: '3:00AM',
        action: 'cath',
        amount: '6 oz',
        details: 'why'
    },
    {
        time: '4:00AM',
        action: 'cath',
        amount: '6 oz',
        details: 'why'
    },
    {
        time: '5:00AM',
        action: 'cath',
        amount: '6 oz',
        details: 'why'
    },
    {
        time: '6:00AM',
        action: 'cath',
        amount: '6 oz',
        details: 'why'
    },
    {
        time: '7:00AM',
        action: 'cath',
        amount: '6 oz',
        details: 'why'
    },
    {
        time: '8:00AM',
        action: 'cath',
        amount: '6 oz',
        details: 'why'
    },
];

function start () {
    document.getElementsByTagName('footer')[0].textContent = 'Copyright 2018';
    console.log(`Welcome`);
    populateLines(entries);
    enableClearAndAdd();
    enableEditAndDelete();
    enableTime();
    enableUnitToggling();
}

function populateLines(entries) {
    const journal = document.getElementById('main');
    for (let entry of entries) {
        const line = document.createElement('ul');
        fillLine(entry, line);
        // line.classList.add(line);
        line.setAttribute('class', 'line');
        let lines = document.querySelectorAll('.line');
        if (lines.length > 2) {
            journal.insertBefore(line, lines[2]);
        } else {
            journal.appendChild(line);
        }
    }
}

function fillLine(entry, line) {
    const time = document.createElement('li');
    time.textContent = entry.time;
    line.appendChild(time);
    
    const action = document.createElement('li');
    action.textContent = entry.action;
    line.appendChild(action);
    
    const amount = document.createElement('li');
    amount.textContent = entry.amount;
    line.appendChild(amount);
    
    const details = document.createElement('li');
    details.textContent = entry.details;
    line.appendChild(details);
    
    const edit = document.createElement('li');
    const editBtn = document.createElement('button');
    editBtn.classList.add('edit');
    editBtn.textContent = 'Edit';
    editBtn.setAttribute('title', 'Edit this entry');
    editBtn.addEventListener('click', editEntry);
    edit.appendChild(editBtn);
    line.appendChild(edit);

    const del = document.createElement('li');
    const delBtn = document.createElement('button');
    delBtn.classList.add('del');
    delBtn.textContent = '–';
    delBtn.setAttribute('title', 'Delete this entry');
    delBtn.addEventListener('click', deleteEntry);
    del.appendChild(delBtn);
    line.appendChild(del);
}

function editEntry (e) {
    console.trace();
}

function deleteEntry (e) {
    console.trace();
}

function enableUnitToggling () {
    // Grab the elements
    const ozs = document.querySelectorAll('.oz');
    const mls = document.querySelectorAll('.ml');
    // Hook some event listeners on the labels
    ozs[1].addEventListener('click', toggleUnits);
    mls[1].addEventListener('click', toggleUnits);
}

function toggleUnits (e) {
    // Grab all the elements
    const allItems = e.target.parentElement.children;
    // Grab the amount of current units and convert
    let amount;
    if (e.target.classList.contains('oz')) {
        console.log(allItems[0].value);
        allItems[2].value = ozToMl(allItems[0].value);
    } else {
        console.log(allItems[2].value);
        allItems[0].value = mlToOz(allItems[2].value);
    }
    // Switch 'em all up
    for (const elm of allItems) {
        elm.classList.toggle('hidden');
    }
}

function ozToMl (oz) {
    return Math.round(oz * 2.957353) * 10;
}

function mlToOz (ml) {
    let res = ml / 29.57353;
    let rem = res - Math.floor(res);
    if (rem >= 0.75) res = Math.ceil(res);
    else {
        res = Math.floor(res);
        if (rem >= 0.25) res += 0.5;
    }
    return res;
}

function enableClearAndAdd () {
    const clear = document.querySelector('input[type="reset"]');
    clear.addEventListener('click', clearInput);

    const add = document.querySelector('input[type="submit"]');
    add.addEventListener('click', addLine);
}

function clearInput (e) {
    let infoBoxes = e.target.parentElement.parentElement.querySelectorAll('input');
    // skipping the last 2 items - which are the buttons that don't have info that we want
    for (let i = 0, len = infoBoxes.length - 2; i < len; i++) {
        // added another input button, so checking for button and skipping them
        if (infoBoxes[i].type !== 'button') {
            if (infoBoxes[i].type !== 'button') {
                infoBoxes[i].value = '';
            }
        }
    }
    // rats forgot that the dropdown is not "input"
    e.target.parentElement.parentElement.querySelector('select').selectedIndex = 0;
    
    // for the time input 
    // show the default time and hide the input
    document.getElementById('timeDisplay').classList.remove('hidden');
    document.getElementById('timeInput').classList.add('hidden');
    document.getElementById('editDate').classList.add('hidden');
}

function addLine (e) {
    const newLine = document.createElement('ul');
    const newInfo = {
        time: getTime(document.getElementById('timeLi')),
        action: getAction(document.getElementById('actionLi')),
        amount: getAmount(document.getElementById('amountLi')),
        details: getDetails(document.getElementById('detailsLi'))
    };
    fillLine(newInfo, newLine);
    newLine.setAttribute('class', 'line');
    const whereTo = document.querySelectorAll('.line')[2];
    whereTo.parentElement.insertBefore(newLine, whereTo);
    clearInput(e);
}

function enableEditAndDelete () {
    console.trace();
}

function enableTime () {
    const timeDiv = document.getElementById('timeDisplay');
    updateTime();

    timeDiv.addEventListener('click', toggleTimeEdit);
    const intervalID = window.setInterval(updateTime, 15000);
    function updateTime () {
        // console.trace();
        let t = new Date().toLocaleTimeString(),
            numIndex = t.lastIndexOf(':');
        timeDiv.textContent = t.substring(0, numIndex) + t.substring(t.length - 2);
    }
}

function toggleTimeEdit (e) {
    document.getElementById('timeDisplay').classList.toggle('hidden');
    document.getElementById('timeInput').classList.toggle('hidden');
    document.getElementById('editDate').classList.toggle('hidden');
}

function getTime (elm) {
    if (elm.children[1].value !== '') {
        const enteredTime = elm.children[1].value.split(':');
        const t = new Date();
        t.setHours(enteredTime[0]);
        t.setMinutes(enteredTime[1]);
        const s = t.toLocaleTimeString();
        const i = s.lastIndexOf(':');
        return s.substring(0, i) + s.substring(s.length - 2);
    } else {
        return elm.children[0].textContent;
    }
}

function getAction (elm) {
    const idx = elm.firstElementChild.selectedIndex;
    return elm.firstElementChild.children[idx].value;
}

function getAmount (elm) {
    // error checking??
    // return the active units
    const oz = document.getElementById('amountOz');
    const ml = document.getElementById('amountMl');
    if (oz.classList.contains('hidden')) {
        // use the ml
        return ml.value !== '' ? ml.value + ' mL' : '';
    } else {
        // use ounces
        return oz.value !== '' ? oz.value + ' oz' : '';
    }
}

function getDetails (elm) {
    return elm.firstElementChild.value;
}
